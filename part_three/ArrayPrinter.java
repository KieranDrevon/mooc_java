package part_three;


public class ArrayPrinter {

    public static void main(String[] args) {
        // You can test your method here
        int[] array = {5, 1, 3, 4, 2};
        printNeatly(array);
    }

    public static void printNeatly(int[] array) {
        // Write some code in here
        String result = "";
        int index = 0;
        
        while (index < array.length) {
            if (array.length - 1 == index) {
                result += array[index];
                break;
            }
            result = result + array[index] + ", ";
            index++;
        }
        System.out.println(result);
    }
}