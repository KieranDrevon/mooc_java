package part_three;
import java.util.Scanner;

public class PersonalDetails {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int count = 0;
        String name = "";
        
        while (true) {
            
            String input = scanner.nextLine();
            
            if (input.isEmpty()) {
                break;
            }
            
            String[] person = input.split(",");
            sum += Integer.valueOf(person[1]);
            count += 1;
            
            if (person[0].length() > name.length()) {
                name = person[0];
            }
        }
        
        scanner.close();
        System.out.println("Longest name: " + name);
        System.out.println("Average of the birth years: " + (1.0 * sum / count));

    }
}
