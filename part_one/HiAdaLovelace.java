package part_one;

public class HiAdaLovelace {

    public static void main(String[] args) {
        String name = "Ada Lovelace";
        System.out.println("Hi " + name + '!');


    }
}


 // a simple challenge going over string concatenation. I did not post about 4 or 5 exercises
 // from yesterday still in part 1 as they are basic for my current level of knowledge.
 // it is true, once you know some foundations of programming, you can see it in all languages
 // I spent a lot of time with ChatGPT yesterday as well going over OOP basics and learning about the keywords 
 // static, void, public and private. I will probably only share this exercise as well and skip a few
 // until the challenges get a bit tougher. 
