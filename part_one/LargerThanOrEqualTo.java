package part_one;
import java.util.Scanner;

public class LargerThanOrEqualTo {
    static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        
        System.out.println("Give the first number:");
        int first = Integer.valueOf(scan.nextLine());
        System.out.println("Give the second number:");
        int second = Integer.valueOf(scan.nextLine());
        if (first > second) {
            System.out.println("Greater number is: " + first);
        } else if (first == second) {
            System.out.println("The numbers are equal!");
        } else {
            System.out.println("Greater number is: " + second);
        }
        scan.close();
    }
}
