package part_four;
import java.util.ArrayList;
import java.util.Scanner;
import part_four.Models.Item;


public class Items {

    public static void main(String[] args) {
        // implement here your program that uses the class Item

        ArrayList<Item> items = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        
        while (true) {
            System.out.println("Name:");
            String name = scanner.nextLine();
            if (name.isEmpty()) {
                break;
            }
            
            Item item = new Item(name);
            
            items.add(item);
            
        }
        
        scanner.close();
        
        for (Item product: items) {
                System.out.println(product.toString());
            }

    }
}
