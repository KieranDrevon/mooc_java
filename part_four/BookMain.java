package part_four;
import part_four.Models.Book;
import java.util.ArrayList;
import java.util.Scanner;

public class BookMain {

    public static void main(String[] args) {
        
        // implement here the program that allows the user to enter 
        // book information and to examine them
        Scanner scanner = new Scanner(System.in);
        ArrayList<Book> books = new ArrayList<>();

        while (true) {
            System.out.println("Title:");
            String title = scanner.nextLine();
            
            if (title.isEmpty()) {
                break;
            }
            
            System.out.println("Pages:");
            int pages = Integer.valueOf(scanner.nextLine());
            
            System.out.println("Publication year:");
            int publicationYear = Integer.valueOf(scanner.nextLine());
            
            books.add(new Book(title, pages, publicationYear));
        }
        
        System.out.println("What information will be printed?");
        String answer = scanner.nextLine();
        scanner.close();
        
        if (answer.equals("everything")) {
            for (Book book: books) {
                System.out.println(book.toString());
        }
            
        } else if (answer.equals("name")) {
            for (Book book: books) {
                System.out.println(book.getTitle());
        }
        }
        
    }
    
}
