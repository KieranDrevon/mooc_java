package part_two;
import java.util.Scanner;

public class Reprint {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many times?");
        int num = Integer.valueOf(scanner.nextLine());
        // int i = 1;

//        for (int i = 1; i <= num;i++) {
//            printText();
//        }

//        while (i <= num) {
//            printText();
//            i++;
//        }

          while ( num > 0 ) {
              printText();
              num--;
          }
          scanner.close();
    }
    
    public static void printText() {
        // write some code here
        System.out.println("In a hole in the ground there lived a method");
    }
}
