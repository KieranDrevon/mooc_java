package part_two;
import java.util.Scanner;
// Another grind of a day, just want to get something up.... Happy 4th
public class CountingToHundred {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = Integer.valueOf(scanner.nextLine());
        
        for (int i = num; i <= 100; i++) {
            System.out.println(i);
        }
        scanner.close();
    }
}
